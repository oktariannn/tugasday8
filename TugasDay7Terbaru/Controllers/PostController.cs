﻿using System.Data.SqlClient;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TugasDay7Terbaru.Model;

namespace TugasDay7Terbaru.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public PostController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost]
        [Route("AddUserWithTask")]
        public string AddUserWithTask(InputModel? inputs)
        {
            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string queryInsertUser = "Insert into Users values('" + inputs.name + "'); SELECT SCOPE_IDENTITY();";
                SqlCommand cmd = new SqlCommand(queryInsertUser, conn);

                var isertedID = cmd.ExecuteScalar();

                foreach (TaskModel tModel in inputs.taks)
                {
                    string queryInsertTasks = "Insert into Tasks values('" + tModel.task_detail + "'," + isertedID + ")";
                    cmd = new SqlCommand(queryInsertTasks, conn);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();

            }
            return "Item Added Successfully";

        }
    }
}

