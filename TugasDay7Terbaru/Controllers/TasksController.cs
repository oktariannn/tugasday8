﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using TugasDay7Terbaru.Model;

namespace TugasDay7Terbaru.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TasksController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [Authorize]
        [HttpGet]
        [Route("GetUserWithTask")]
        public List<OutputModel> GetUserWithTask(string? name)
        {
            List<OutputModel> output = new List<OutputModel>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string queryUser = "SELECT * FROM Users";
                SqlCommand cmd = new SqlCommand(queryUser, conn);

                if (!string.IsNullOrEmpty(name))
                {
                    queryUser = "SELECT * FROM Users WHERE name = @name";
                    cmd = new SqlCommand(queryUser, conn);
                    cmd.Parameters.AddWithValue("@name", name);
                }

                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dtUser = new DataTable();
                sqlDataAdapter.Fill(dtUser);

                string queryTasks = "SELECT * FROM Tasks";
                SqlCommand cmdTasks = new SqlCommand(queryTasks, conn);
                sqlDataAdapter = new SqlDataAdapter(cmdTasks);
                DataTable dtTasks = new DataTable();
                sqlDataAdapter.Fill(dtTasks);

                for (int i = 0; i < dtUser.Rows.Count; i++)
                {
                    OutputModel oModel = new OutputModel();
                    oModel.pk_users_id = Convert.ToInt32(dtUser.Rows[i]["pk_users_id"].ToString());
                    oModel.name = dtUser.Rows[i]["name"].ToString();
                    List<TaskModel> listTask = new List<TaskModel>();

                    for (int j = 0; j < dtTasks.Rows.Count; j++)
                    {
                        if (dtUser.Rows[i]["pk_users_id"].ToString() == dtTasks.Rows[j]["fk_users_id"].ToString())
                        {
                            TaskModel tModel = new TaskModel();
                            tModel.pk_tasks_id = Convert.ToInt32(dtTasks.Rows[j]["pk_tasks_id"].ToString());
                            tModel.task_detail = dtTasks.Rows[j]["task_detail"].ToString();
                            listTask.Add(tModel);
                        }
                    }

                    oModel.tasks = listTask;
                    output.Add(oModel);
                }
            }

            return output;
        }
    }
}
