﻿namespace TugasDay7Terbaru.Model
{
    public class TaskModel
    {
        public int? pk_tasks_id { get; set; }
        public string? task_detail { get; set; }
        public int? fk_user_id { get; set; }
    }
}
